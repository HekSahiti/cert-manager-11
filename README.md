# cert-manager-11
A K8s configuration to install and configure cert-manager v0.11.0.

Then execute the deploy.sh script.

If you need to deploy the files manually, this is the order:

01. cert-manager-crd.yaml
02. cert-manager-ns.yaml
03. cert-manager-sa.yaml
04. cert-manager-cr.yaml
05. cert-manager-crb.yaml
06. cert-manager-role.yaml
07. cert-manager-rb.yaml
08. cert-manager-svc.yaml
09. cert-manager-deploy.yaml
10. cert-manager-apisvc.yaml
11. cert-manager-mwc.yaml
12. cert-manager-vwc.yaml
